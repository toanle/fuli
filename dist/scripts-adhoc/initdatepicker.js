var datepicker = {
	initDateRangePicker: function (startDate, endDate) {
		$('.date-picker').daterangepicker({
				autoApply: true,
				startDate: startDate || '29/9/2018',
                endDate: endDate || '05/10/2018',
                locale: {
                    format: 'DD/MM/YYYY'
                },
				singleDatePicker: $(this).hasClass('date-picker--single'),
				showDropdowns: $(this).hasClass('date-picker--single'),
				minDate: moment('1901-01-01'),
				maxDate: moment().endOf('year')
			},
			function (start, end, label) {
				console.log(
					'Selected date range: From ' +
					start.format('DD/MM/YYYY') +
					', To ' +
					end.format('DD/MM/YYYY')
				);
			}
		);
	}
};

$(function() {
    // datepicker.initDateRangePicker('25/9/2018', '28/9/2018');
});
