var validator = {
    formValidation: function() {
        $('.j-form-validation').validate({
            errorElement: 'span',
            rules: {
                'signup-phone': {
                    required: true,
                    number: true
                },
                'change-pass-verify': {
                    equalTo: '#change-pass-new'
                }
            },
            invalidHandler: function(form, validator) {
                setTimeout(function() {
                    articles.resetLoadingBtn(
                        $(validator.currentForm).find('button')
                    );
                }, 0);
            },
            submitHandler: function(form) {
                console.log('form', form);
                form.submit();
                setTimeout(function() {
                    console.log(
                        'form submited from main.js file, inside function formValidation'
                    );
                    articles.resetLoadingBtn($(form).find('button'));
                }, 2000);
            }
        });
    }
};
$(function() {
    validator.formValidation();
});