var contentExpand = {
    toggleContent: function() {
        $('body').on('click', '.content-expand__title', function() {
            var contentContainer = $(this).closest('.content-expand');
            contentContainer.toggleClass('collapse');
            contentContainer.find('ul').stop().slideToggle();
        });
    }
}