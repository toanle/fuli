var showMoreProps = {
	btn: '.btn--loading',
	btnStateClassName: 'btn--is-loading',
	btnTextOriginal: '',
	btnTextReplace: 'Đang Tải. . .'
}
var articles = {
    resetLoadingBtn: function (clickedBtn) {
        clickedBtn.find('.btn__text').text(showMoreProps.btnTextOriginal);
        clickedBtn.removeClass(showMoreProps.btnStateClassName);
        clickedBtn.width('auto');
    },
	showMoreData: function () {
		$('body').on('click', showMoreProps.btn, function () {
			var self = $(this);
			self.width(self.width());
			showMoreProps.btnTextOriginal = self.text();
			self.find('.btn__text').text(showMoreProps.btnTextReplace);
            self.addClass(showMoreProps.btnStateClassName);
			// setTimeout(function () {
            //     articles.resetLoadingBtn(self);
			// }, 2000); // hardcode for development, should be removed in production
		});
	},
}
