var articleCarousel = {
	initCarousel: function () {
		var articleCarousel = '.article-carousel';
		$(articleCarousel).each(function () {
            var itemPerRow = $(this).attr('item-per-row') || 4;
			var carousel = new Swiper(articleCarousel + ' .swiper-container', {
				loop: true,
				navigation: {
					nextEl: '.article-carousel .swiper-button-next',
					prevEl: '.article-carousel .swiper-button-prev',
				},
				slidesPerView: itemPerRow,
				spaceBetween: 20,
				breakpoints: {
					1024: {
						slidesPerView: itemPerRow - 1,
					},
					768: {
						slidesPerView: 2,
					},
					576: {
						slidesPerView: 1
					}
				}
			});
		});
	}
};
