var payment = {
    togglePaymentMethod: function() {
        $('body').on('click', '.payment-method', function() {
            $(this).toggleClass('active');
            $(this).siblings('.payment-method-detail').stop().slideToggle();
            $(this).parent().siblings('.payment-method-wrapper').find('.payment-method').removeClass('active');
            $(this).parent().siblings('.payment-method-wrapper').find('.payment-method-detail').slideUp();
        });
    }
};