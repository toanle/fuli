// footer scripts
var footer = {
	initAccordion: function () {
		$('body').on('click', '.footer__accordion', function () {
			if ($(window).width() < 768) {
				$(this).toggleClass('is-show');
				$(this).find('ul').stop().slideToggle();
				$(this).siblings('.footer__accordion').find('ul').stop().slideUp();
			}
		});
    }
}
