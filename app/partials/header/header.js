var header = {
    toggleNav: function() {
        // $('body').on('click', '.header__categories-trigger', function () {
        // 	$(this).toggleClass('show');
        //     var categories = $(this).find('.categories');
        //     categories.slideToggle();
        // 	// var scrollH = $(this).hasClass('show') ? categories.prop('scrollHeight') : 0;
        // 	// categories.height(scrollH);
        // });
        // $(document).click(function (e) {
        //     var container = $('.header__categories-trigger.show');
        // 	if (!container.is(e.target) && container.has(e.target).length === 0) {
        // 		container.click(); // toggle click to hide the dropdown
        // 	}
        // });
    },
    showCategories: function() {
        $('.header__categories-trigger .categories .category').each(function(index) {
            var _index = index + 1;
            $(this).addClass('small-delay-' + _index);
        });

        $('.category .category-topics').each(function(index) {
            $(this)
                .find('.category-topics__item')
                .each(function(i) {
                    var _index = i + 1;
                    $(this).addClass('small-delay-' + _index);
                });
        });
    },
    mobileAccordion: function() {
        $('.menu-mobile').on('click', '.m-accordion', function() {
            if ($(window).width() <= breakpoint.desktop) {
                $(this).toggleClass('is-show');
                $('.menu-mobile .categories').stop().slideToggle();
            }
        });
        $('.menu-mobile').on('click', '.category.has-child-content .category__title', function() {
            if ($(window).width() <= breakpoint.desktop) {
                $(this).parent().toggleClass('is-show');
                $(this).parent().find('.category-topics').stop().slideToggle();
                $(this).parent().siblings('.category').removeClass('is-show').find('.category-topics').stop().slideUp();
            }
        });
    },
    changeElementsPlace: function() {
        var menuMobile = $('.menu-mobile'),
            mAccordion = $('.m-accordion'),
            categories = $('.categories-wrapper'),
            enableCourse = $('.header__enable-course'),
            userProfile = $('.header-userprofile'),
            desktopNav = $('.header__categories-trigger')
        if($(window).width() <= breakpoint.desktop) {
            menuMobile.append(mAccordion).append(categories).append(enableCourse).append(userProfile);
        }else {
            desktopNav.append(mAccordion).append(categories);
            $('.header').append(enableCourse).append(userProfile);
        }
    },
    toggleMenuMobile: function() {
        $('body').on('click', '.hamburger-icon', function() {
            $('.menu-mobile').toggleClass('is-show');
        });
    },
    closeMenu: function() {
        $('body').on('click', '.menu-mobile__close', function() {
            $('.menu-mobile').removeClass('is-show');
        });
        $(document).click(function (e) {
            var container = $('.header');
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				$('.menu-mobile').removeClass('is-show');
			}
		});
    }
};
