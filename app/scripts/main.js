'use strict';
jQuery.extend(jQuery.validator.messages, {
    required: 'Vui lòng điền thông tin.',
    remote: 'Please fix this field.',
    email: 'Vui lòng nhập chính xác email.',
    url: 'Please enter a valid URL.',
    date: 'Please enter a valid date.',
    dateISO: 'Please enter a valid date (ISO).',
    number: 'Vui lòng chỉ nhập số (0->9)',
    digits: 'Please enter only digits.',
    creditcard: 'Please enter a valid credit card number.',
    equalTo: 'Mật khẩu mới phải giống nhau',
    accept: 'Please enter a value with a valid extension.',
    maxlength: jQuery.validator.format(
        'Please enter no more than {0} characters.'
    ),
    minlength: jQuery.validator.format('Vui lòng nhập ít nhất {0} ký tự.'),
    rangelength: jQuery.validator.format(
        'Please enter a value between {0} and {1} characters long.'
    ),
    range: jQuery.validator.format('Please enter a value between {0} and {1}.'),
    max: jQuery.validator.format(
        'Please enter a value less than or equal to {0}.'
    ),
    min: jQuery.validator.format(
        'Please enter a value greater than or equal to {0}.'
    )
});
var isDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
    ),
    isAndroid = /Android/i.test(navigator.userAgent),
    isIos = /iPhone|iPad|iPod/i.test(navigator.userAgent),
    isIE11 = !!(
        navigator.userAgent.match(/Trident/) &&
        navigator.userAgent.match(/rv[ :]11/)
    ),
    isMobile = $(window).width() < 768;
var timing = {
    easingTime: 700
};
var breakpoint = {
    mobile: 767,
    desktop: 1024
};
var global = {
    detectDevices: function() {
        var a = isDevice === true ? ' device' : ' pc',
            b = isAndroid === true ? ' android' : ' not-android',
            c = isIos === true ? ' ios' : ' not-ios',
            d = isMobile ? ' mobile' : ' desktop',
            e = isIE11 ? ' ie11' : ' ',
            htmlClass = a + b + c + d + e;
        $('html').addClass(htmlClass);
        if (isDevice) $('body').css('cursor', 'pointer');
    },
    showElement: function() {
        if ($('.wrapper .animated').length) {
            $('.wrapper .animated').each(function() {
                var viewportBottom = $(window).scrollTop() + $(window).height();
                var elementOffsetTop = $(this).offset().top;
                var elementHeight = $(this).outerHeight();
                var maxOffsetToShowElement = isMobile ? 100 : 200;
                if (
                    viewportBottom > elementOffsetTop + elementHeight ||
                    viewportBottom > elementOffsetTop + maxOffsetToShowElement
                ) {
                    var animatedClass =
                        $(this).attr('animated-class') || 'fadeInUp';
                    $(this).addClass(animatedClass);
                }
            });
        }
    },
    replaceImgToBackground: function(img) {
        $.each(img, function() {
            if ($(this).css('visibility') == 'visible') {
                $(this)
                    .parent()
                    .css(
                        'background-image',
                        'url(' + $(this).attr('src') + ')'
                    );
            }
        });
    },
    overflowBody: function() {
        $('html, body').css({
            height: '100%',
            overflow: 'hidden'
        });
    },
    clearOverflowBody: function() {
        $('html, body').css({
            height: 'auto',
            overflow: 'visible'
        });
    },
    scrollTo: function(offset, easingTime) {
        $('html, body').animate(
            {
                scrollTop: offset
            },
            easingTime
        );
    },
    toggleDropdown: function() {
        $('body').on('click', '.dropdown', function() {
            $(this).toggleClass('show');
            var dropdownMenu = $(this).find('.dropdown-menu');
            var scrollH = $(this).hasClass('show')
                ? dropdownMenu.prop('scrollHeight')
                : 0;
            dropdownMenu.height(scrollH);
        });
        $(document).click(function(e) {
            var container = $('.dropdown.show');
            if (
                !container.is(e.target) &&
                container.has(e.target).length === 0
            ) {
                container.click(); // toggle click to hide the dropdown
            }
        });
    },
    showSplittedText: function(element) {
        var strArray = element
            .text()
            .split('')
            .reverse();
        element.text('');
        element.css('opacity', 1);
        var outputSlowly = setInterval(function() {
            element.append(strArray.pop());
            if (strArray.length === 0) {
                clearInterval(outputSlowly);
            }
        }, 70);
    },
    truncateArticleText: function() {
        $('.article .article-content-info p').dotdotdot({
            ellipsis: '\u2026 '
        });
        $('.article .article-detail-course-about p').dotdotdot({
            ellipsis: '\u2026 '
        });
    },
    playVideo: function() {
        $('body').on('click', '.icon-play', function() {
            var container = $(this).closest('.iframe-responsive');
            var iframe = container.find('iframe')[0];
            var player = $f(iframe);
            var thumb = container.find('.iframe-responsive__img');
            player.api('play');
            $(this).fadeOut(600);
            thumb.fadeOut(600);
        });
    },
    handleSectionAnchor: function() {
        $('.section-anchor').on('click', 'li', function(e) {
            // $(this).addClass('active');
            // $(this).siblings().removeClass('active');
            var target = $(this).attr('target');
            global.scrollTo(
                $(target).offset().top -
                    $('.header').outerHeight() -
                    $('.section-anchor').outerHeight(),
                timing.easingTime
            );
        });
    },
    handleSidebar: function() {
        var sidebar = $('.sidebar-left aside');
        if (sidebar.length) {
            var sidebarBrief = sidebar
                .closest('.layout-sidebar')
                .find('.layout-sidebar-brief');
            sidebar.css(
                'margin-top',
                sidebarBrief.css('padding-top').replace('px', '') * 1 -
                    sidebarBrief.outerHeight()
            );
        }
    },
    stickyInParent: function() {
        $('.sidebar-left aside').stick_in_parent({
            offset_top: $('.header').outerHeight()
        });
        $('.section-anchor').stick_in_parent({
            offset_top: $('.header').outerHeight()
        });
    },
    scrollSectionAnchor: function() {
        var scrollTop = $('html').scrollTop();
        $('.section-anchor li').each(function() {
            var target = $(this).attr('target');
            if (
                scrollTop >
                $(target).offset().top -
                    ($('.header').outerHeight() +
                        $('.section-anchor').outerHeight())
            ) {
                $('.section-anchor li').removeClass('active');
                $(this).addClass('active');
            }
        });
    },
    setArticleIndex: function() {
        for (var i = $('.article').length; i >= 0; i--) {
            var article = $('.article')[$('.article').length - i];
            $(article)
                .parent()
                .css({
                    position: 'relative',
                    'z-index': i
                });
        }
    },
    toggleCouponFields: function() {
        $('body').on('click', '.coupon-links .coupon-links__trigger', function(
            e
        ) {
            e.preventDefault();
            var self = $(this);
            $(this)
                .parent()
                .find('.coupon-links__input-wrapper')
                .stop()
                .slideToggle(400, function() {
                    self.parent()
                        .find('.coupon-links__input')
                        .focus();
                });
        });
    },
    readUrl: function(input) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.profile-user__img img').attr('src', e.target.result);
        };
        if (input.files && input.files[0]) {
            reader.readAsDataURL(input.files[0]);
        }
    },
    uploadImg: function() {
        $('.j-input-upload').on('change', function() {
            global.readUrl(this);
        });
    },
    viewPass: function() {
        $('.form-group--has-view-pass').on('click', '.view-pass', function() {
            var input = $(this)
                .closest('.form-group')
                .find('input');
            var inputType = input.attr('type');
            if (inputType === 'password') {
                input.attr('type', 'text');
            } else {
                input.attr('type', 'password');
            }
        });
    },
    init: function() {},
    load: function() {},
    resize: function() {}
};
