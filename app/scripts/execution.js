$(function () {
    global.detectDevices();
    global.toggleDropdown();
    global.showElement();
    header.showCategories();
    header.mobileAccordion();
    header.changeElementsPlace();
    header.toggleMenuMobile();
    header.closeMenu();
    articleCarousel.initCarousel();
    footer.initAccordion();
    articles.showMoreData();
    global.showSplittedText($('.hero-banner-content h1'));
    global.truncateArticleText();
    global.uploadImg();
    global.handleSectionAnchor();
    global.setArticleIndex();
    global.viewPass();
    global.handleSidebar();
    global.playVideo();
    global.stickyInParent();
    global.toggleCouponFields();
    contentExpand.toggleContent();
    payment.togglePaymentMethod();
    learning.toggleAssetsTab();
    learning.playSelectedVideo();
    learning.setStateBtn();
    learning.toggleFullView();
});
$(window).on('load', function (e) {
    //
});
var width = $(window).width();
var resize = 0;
$(window).resize(function () {
	var _self = $(this);
	resize++;
	setTimeout(function () {
		resize--;
		if (resize === 0) { // Done resize ...
			if (_self.width() !== width) { // Done resize only width ...
                header.changeElementsPlace();
                global.handleSidebar();
			}
		}
	}, 100);
});
$(window).scroll(function () {
	global.showElement();
	global.scrollSectionAnchor();
});
