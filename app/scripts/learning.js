var learning = {
	toggleAssetsTab: function () {
		$('body').on('click', '.learning-assets__title li', function () {
			$(this).addClass('active');
			$(this).siblings().removeClass('active');
			var target = $(this).attr('target');
			$(target).siblings('.learning-asset-content').fadeOut(200, function () {
				$(target).fadeIn(400);
			});
		});
	},
	playVideoCourse: function (videoId) {
		var iframeWrapper = $('.learning-video__main-video');
		iframeWrapper.find('iframe').attr('src', 'https://player.vimeo.com/video/' + videoId + '?api=1&autoplay=1');
		learning.setStateBtn();
		// var iframe = iframeWrapper.find('iframe')[0];
		// console.log('iframe', iframe);
		// var player = $f(iframe);
		// console.log('player', player);
		// player.api('play');
	},
	playSelectedVideo: function () {
		$('.learning-assets-curriculum').on('click', 'li', function () {
			var videoId = $(this).attr('video-id');
			$('.learning-assets-curriculum li.active').removeClass('active').addClass('done');
			$(this).addClass('active');
			learning.playVideoCourse(videoId);
		});
		$('body').on('click', '.previous-course', function () {
			var currentCourseIndex = $('.learning-assets-curriculum li').index($('.learning-assets-curriculum li.active'));
			if (currentCourseIndex > 0) {
				$($('.learning-assets-curriculum li')[currentCourseIndex - 1]).click();
			}
		});
		$('body').on('click', '.next-course', function () {
			var currentCourseIndex = $('.learning-assets-curriculum li').index($('.learning-assets-curriculum li.active'));
			if (currentCourseIndex < $('.learning-assets-curriculum li').length - 1) {
				$($('.learning-assets-curriculum li')[currentCourseIndex + 1]).click();
			}
		});
	},
	setStateBtn: function () {
		if ($('.learning-assets-curriculum li.active').length) {
			var currentCourseIndex = $('.learning-assets-curriculum li').index($('.learning-assets-curriculum li.active'));
			$('.learning-video__navigator .btn').removeClass('btn--disabled');
			if (currentCourseIndex === 0) {
				$('.previous-course').addClass('btn--disabled');
			}
			if (currentCourseIndex === $('.learning-assets-curriculum li').length - 1) {
				$('.next-course').addClass('btn--disabled');
			}
		}
	},
	toggleFullView: function () {
		$('body').on('click', '.learning-video-tool__fullview', function () {
			$('.learning').toggleClass('learning--full-view');
		});
	}
}
